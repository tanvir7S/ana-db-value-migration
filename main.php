<?php
require 'functions.php';

$servername = "localhost";
$port = "3306";
$username = "root";
$password = "12345";
$database = "analytics_test_develop";

// Create connection
$conn = new mysqli($servername.':'.$port, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully\n";

$fkCheckDisableQuery = "SET FOREIGN_KEY_CHECKS=0";
if ($conn->query($fkCheckDisableQuery) === TRUE) {
    echo "Foreign key disabled successfully\n";
} else {
    die("Error disabling foreign key: " . $conn->error);
}

$TICKET_TYPE_RELATED_TABLES = [
    'master_mapping_ticket_type' => 'ticket_type_id',
    'master_mapping_ticket_type_return_shipment' => 'ticket_type_id',
    'notification_sending_logic' => 'ticket_type_id',
    'shop_content_library' => 'ticket_type_id',
    'ticket' => 'type_id',
    'ticket_type' => 'parent_id',
    'ticket_type' => 'return_parent_id',
];

$SHIPMENT_STATUS_RELATED_TABLES = [
    'master_mapping' => 'status_id',
    'master_mapping' => 'status_for_return_shipment_id',
    'shipment' => 'shipment_status_id',
    'shipment_status_history' => 'shipment_status_id',
    'shipment_timeline' => 'shipment_status_id',
];

updateRelatedTables($conn, 'ana_prod_ticket_type', 'ticket_type', $TICKET_TYPE_RELATED_TABLES);
updateRelatedTables($conn, 'ana_prod_shipment_status', 'shipment_status', $SHIPMENT_STATUS_RELATED_TABLES);


$fkCheckEnableQuery = "SET FOREIGN_KEY_CHECKS=1";
if ($conn->query($fkCheckEnableQuery) === TRUE) {
    echo "Foreign key enabled successfully\n";
} else {
    die("Error enabling foreign key: " . $conn->error);
}

?>