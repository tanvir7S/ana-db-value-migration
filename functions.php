<?php

function updateRelatedTables($conn, $prodTable, $targetTable, $relatedTables){

    $sql = "SELECT * FROM " . $prodTable;
    if (!$result = $conn->query($sql)) {
        die ('There was an error running query[' . $conn->error . ']');
    }
    
    while ($row = $result->fetch_assoc()) {
        
        // get id & name from prod
        $prodId = $row['id'];
        $prodName = $row['name'];
    
        // get id from current table where name is same
        $currentTableQuery = sprintf("SELECT `id` FROM ". $targetTable ." WHERE name='%s'", $conn->real_escape_string($prodName));
        $currentTableResult = $conn->query($currentTableQuery);
        $currentTableRow = $currentTableResult->fetch_assoc();
        $currentId = $currentTableRow['id'];
        
        if($currentId == null){
            continue;
        }
    
        if($currentId == $prodId){
            continue;
        }
    
        echo 'id:'.$row['id'].'-'.'name:'.$row['name']."\t";
        echo "-CurrentId:".$currentTableRow['id']."\t";
        echo '-it is different'."\t";
    
    
        //update ids in related tables first
        foreach($relatedTables as $relatedTable => $columnName){
            $updateQuery = "UPDATE ".$relatedTable." SET ".$columnName."='".$prodId."' WHERE ".$columnName."='".$currentId."'";
    
            if ($conn->query($updateQuery) === TRUE) {
                echo $relatedTable."-Record updated successfully";
            } else {
                echo $relatedTable."-Error updating record: " . $conn->error;
            }
            echo "\t";
        }
    
        echo "\n";
        
    }
}


?>